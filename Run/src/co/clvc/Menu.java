package co.clvc;

import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.net.MalformedURLException;
import java.net.URL;

import javax.swing.Icon;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.SwingConstants;

@SuppressWarnings("serial")
public class Menu extends JPanel {


	static JFrame frame;
	
	public static void main(String[] args) {
		javax.swing.SwingUtilities.invokeLater(new Runnable() {
			public void run() {
				createAndShowGUI();
			}

		});
	}

	private static void createAndShowGUI() {
		
		frame = new JFrame("Menu");
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.setResizable(false);
		
		Menu menu = new Menu();
		
		menu.setOpaque(true);
		
		frame.setContentPane(menu);
		
		frame.pack();
		frame.setVisible(true);

		
	}
	
	
	public Menu(){
		super(new GridLayout(6,1));
		
		
		URL url = null;
		try {
			url = new URL("http://www.fileize.com/files/970122c6/018/Running_Practice.gif");
		} catch (MalformedURLException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		
		Icon icon = new ImageIcon(url);
		
		
		
		JLabel title = new JLabel("RUN", SwingConstants.CENTER);
		JLabel runGIF = new JLabel(icon);
		
		JButton start = new JButton("Start");
		JButton settings = new JButton("Settings");
		JButton credits = new JButton("Credits");
		JButton endless = new JButton("Endless");
		
		
		title.setFont(title.getFont().deriveFont(70.0f));

		
		add(title);
		add(runGIF);

		add(start);
		add(settings);
		add(credits);
		add(endless);
		
		start.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {

				Main gameContent = new Main();
				gameContent.setOpaque(true);
				frame.setContentPane(gameContent);
				
				frame.pack();
				frame.setVisible(true);
			}

		});
		
		
		
		
		
		
		
		
	}
	

}
