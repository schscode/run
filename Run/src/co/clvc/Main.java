package co.clvc;

//CLVC

/*Header
 * 
 * Project: 2 - Tower Defense
 * 
 * By Colin Cooke
 * 
 * Started on February 13th 2014
 * 
 * Main Class
 * 
 * There is a seperate class in charge of controlling the information to draw the towers/enemies, with a set refresh rate
 * 
 * 
*/

//All Imports needed
import java.awt.Dimension;
import java.awt.Graphics;
import javax.swing.JPanel;

@SuppressWarnings("serial")
public class Main extends JPanel implements Runnable {



	// declaration of the mainThread (in this location for scope)
	Thread mainThread;


	// Declarations of the instances of classes (with the class they will be an
	// instance of as the type).
	// Declared here for scope
	DynamicGamePlay dynamicGamePlay;

	// Constructor Method
	public Main() {

		//setLayout(null); //uncomment if painting doesn't work
	

		// creates and instance of the DynamicGamePlay class
		dynamicGamePlay = new DynamicGamePlay();

		// Creates a thread called main thread (this is what the runnable will
		// be on - see class "run")
		mainThread = new Thread(this, "Main Thread");

		// starts the thread and therefore the runnable class
		mainThread.start();


	}

	// sets the dimensions of the window
	public Dimension getPreferredSize() {
		return new Dimension(1500, 900);
	}

	// This class is where all graphics are drawn
	protected void paintComponent(Graphics g) {
		super.paintComponent(g);


	}

	@SuppressWarnings("static-access")
	@Override
	public void run() {
		// TODO Auto-generated method stub

		while (true) {

			
			//adds a delay by making the thread sleep for 10 ms
			try {
				mainThread.sleep(10);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}

		}

	}

}
