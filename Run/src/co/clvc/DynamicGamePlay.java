package co.clvc;

public class DynamicGamePlay implements Runnable{
	Thread dThread;

	// This array represents the possible tower locations in the game, if true
	// there can be a tower there

	// This is the declaration of the grid, nothing fancy.
	// The Dimensions are 19x16

	// All Values which are true are for the enemy, false are for towers
	// (further restrictions on tower placement may arise)
	



	DynamicGamePlay() {

		dThread = new Thread(this, "Dynamic Thread");

		dThread.start();

	}

	@Override
	public void run() {

		// Generate the enemy path

		// The way this is done is to "draw" straight lines between all corners
		// of the BGrid and add the end points of the straight lines to a
		// ListArray, in this way the method can be changed to draw curves as
		// well (if wanted)



		while (true) {

			// pause needs to be a volatile boolean in order for the jvm not to
			// used its cached value for optimization (due to it only be changed
			// in value by an external class)

				// add a delay
				sleep();
		}

	}

	@SuppressWarnings("static-access")
	private void sleep() {
		try {
			
			//add your delay here
			dThread.sleep(10);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	}
}